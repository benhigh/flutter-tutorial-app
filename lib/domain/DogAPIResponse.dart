import 'dart:convert';

class DogAPIResponse {
  String status;
  Map<String, dynamic> breeds;

  DogAPIResponse({this.status, this.breeds});

  factory DogAPIResponse.fromJson(String input) {
    Map parsed = json.decode(input);

    return DogAPIResponse(
      status: parsed["status"],
      breeds: parsed["message"]
    );
  }
}