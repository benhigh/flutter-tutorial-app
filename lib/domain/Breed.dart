class Breed {
  String name;
  bool favorite;

  Breed({this.name, this.favorite});

  factory Breed.fromMap(Map<String, dynamic> map) {
    return Breed(
      name: map["name"],
      favorite: map["favorite"] > 0
    );
  }

  Map<String, dynamic> toMap() => {
    "name": name,
    "favorite": favorite
  };
}
