import 'package:flutter/material.dart';
import 'package:my_flutter_app/db/repo/BreedRepo.dart';
import 'package:my_flutter_app/domain/Breed.dart';
import 'package:my_flutter_app/domain/DogAPIResponse.dart';
import 'package:my_flutter_app/widgets/DogBreed.dart';
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());

final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      navigatorObservers: <NavigatorObserver>[routeObserver],
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with RouteAware {
  List<dynamic> _dogs = new List();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();
  bool _refreshEnabled = true;

  @override
  void initState() {
    super.initState();
    refreshDogs();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context));
  }

  @override
  void didPopNext() {
    super.didPopNext();
    refreshDogs();
  }

  void refreshDogs() async {
    setState(() {
      _refreshEnabled = false;
      _dogs.clear();
    });

    http.Response response = await http.get("https://dog.ceo/api/breeds/list/all");
    if (response.statusCode != 200) {
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("Getting failed with code " + response.statusCode.toString())
        )
      );

      setState(() {
        _refreshEnabled = true;
      });
      return;
    }

    DogAPIResponse apiResponse = DogAPIResponse.fromJson(response.body);
    if (apiResponse.status != "success") {
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("Dog API said the request status was " + apiResponse.status)
        )
      );

      setState(() {
        _refreshEnabled = true;
      });
      return;
    }

    BreedRepo repo = new BreedRepo();

    List<Breed> breedsToAdd = new List();
    for (MapEntry<String, dynamic> entry in apiResponse.breeds.entries) {
      Breed breed = new Breed(name: entry.key);
      breed.favorite = await repo.isFavorite(entry.key);
      breedsToAdd.add(breed);
    }

    setState(() {
      _dogs.addAll(breedsToAdd);
      _refreshEnabled = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: _refreshEnabled ? refreshDogs : null,
          ),
        ],
      ),
      body: ListView(
        children: _dogs
          .map((d) => new DogBreed(d))
          .toList(growable: false),
      )
    );
  }
}

