import 'package:my_flutter_app/db/DBConnector.dart';
import 'package:my_flutter_app/domain/Breed.dart';
import 'package:sqflite/sqflite.dart';

class BreedRepo {
  Database _db;

  _setUpDb() async {
    if (_db == null) {
      DBConnector connector = new DBConnector();
      _db = await connector.database;
    }
  }

  Future<List<Breed>> getBreeds() async {
    await _setUpDb();

    List<Map<String, dynamic>> results = await _db.query("Favorites",
      columns: ["name", "favorite"]);

    List<Breed> breeds = new List();
    for (Map<String, dynamic> result in results) {
      breeds.add(Breed.fromMap(result));
    }

    return breeds;
  }

  Future<bool> isFavorite(String name) async {
    await _setUpDb();

    List<Map<String, dynamic>> result = await _db.query("Favorites",
      columns: ["favorite"], where: "name = ?", whereArgs: [name]);

    if (result.isEmpty) {
      return false;
    } else {
      return result[0]["favorite"] == 1;
    }
  }

  Future<int> upsert(Breed breed) async {
    await _setUpDb();

    List<Map<String, dynamic>> results = await _db.query("Favorites",
      where: "name = ?", whereArgs: [breed.name]);

    if (results.isEmpty) {
      return await _db.insert("Favorites", breed.toMap());
    } else {
      return await _db.update("Favorites", breed.toMap(),
        where: "name = ?", whereArgs: [breed.name]);
    }
  }
}