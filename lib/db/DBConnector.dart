import 'dart:io';

import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class DBConnector {
  DBConnector();
  static final DBConnector db = DBConnector();

  static Database _database;

  Future<Database> get database async {
    if (_database == null) {
      _database = await initDB();
    }

    return _database;
  }

  initDB() async {
    Directory dir = await getApplicationDocumentsDirectory();
    String path = join(dir.path, "Database.db");

    return await openDatabase(
      path,
      version: 1,
      onOpen: (db) {},
      onCreate: (Database db, int version) async {
        await db.execute("CREATE TABLE Favorites("
          "name TEXT PRIMARY KEY,"
          "favorite BIT NOT NULL"
          ")");
      }
    );
  }
}