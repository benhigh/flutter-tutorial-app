import 'package:flutter/material.dart';
import 'package:my_flutter_app/domain/Breed.dart';
import 'package:my_flutter_app/widgets/DogBreedHeader.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class DogDetails extends StatefulWidget {
  final Breed _breed;

  DogDetails(this._breed);

  @override
  State<StatefulWidget> createState() {
    return _DogDetails(this._breed);
  }
}

class _DogDetails extends State<DogDetails> {
  Breed _breed;
  FadeInImage _image;
  FlutterLocalNotificationsPlugin _notifications;

  _DogDetails(this._breed);

  @override
  void initState() {
    super.initState();
    getDogImage();
    
    var androidSettings = new AndroidInitializationSettings('@mipmap/ic_launcher');
    var iOSSettings = new IOSInitializationSettings();
    var notificationSettings = new InitializationSettings(
      androidSettings, iOSSettings
    );

    _notifications = new FlutterLocalNotificationsPlugin();
    _notifications.initialize(notificationSettings,
      onSelectNotification: _notificationHandler
    );
  }

  Future _notificationHandler(String payload) async {
    showDialog(
      context: context,
      builder: (_) => new AlertDialog(
        title: new Text("Notification!"),
        content: new Text(payload),
      )
    );
  }

  void _showNotification() async {
    var channel = new AndroidNotificationDetails(
      "dog_notifications",
      "Dog Notifications",
      "Notifications about dogs",
    );
    var iosDetails = new IOSNotificationDetails();

    NotificationDetails details = new NotificationDetails(channel, iosDetails);

    await _notifications.show(
      0,
      "You wanted to be notified about this dog!",
      "You asked for a notification about " + _breed.name + ", so here it is!",
      details,
      payload: _breed.name
    );
  }

  void getDogImage() async {
    String url = "https://dog.ceo/api/breed/" + _breed.name + "/images/random";

    http.Response response = await http.get(url);
    if (response.statusCode != 200) {
      setState(() {
        _image = FadeInImage(
          placeholder: AssetImage("assets/loadingImage.png"),
          image: AssetImage("assets/errorImage.png")
        );
      });
      return;
    }

    Map<String, dynamic> responseMap = json.decode(response.body);
    if (responseMap["status"] != "success") {
      setState(() {
        _image = FadeInImage(
          placeholder: AssetImage("assets/loadingImage.png"),
          image: AssetImage("assets/errorImage.png")
        );
      });
      return;
    }

    setState(() {
      _image = FadeInImage.assetNetwork(
        placeholder: "assets/loadingImage.png",
        image: responseMap["message"]
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_breed.name),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.notifications),
            tooltip: "Show Notification",
            onPressed: _showNotification,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            DogBreedHeader(breed: _breed),
            _image ?? Image.asset("assets/loadingImage.png")
          ],
        ),
      ),
    );
  }
}