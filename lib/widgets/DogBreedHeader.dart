import 'package:flutter/material.dart';
import 'package:my_flutter_app/db/repo/BreedRepo.dart';
import 'package:my_flutter_app/domain/Breed.dart';

class DogBreedHeader extends StatefulWidget {
  final Breed breed;

  DogBreedHeader({key,
    @required this.breed
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _DogBreedHeader(breed);
  }
}

class _DogBreedHeader extends State<DogBreedHeader> {
  Breed _breed;
  bool _favoriteButtonEnabled = true;

  _DogBreedHeader(this._breed);

  void favoriteBreed() async {
    setState(() {
      _favoriteButtonEnabled = false;
      _breed.favorite = !_breed.favorite;
    });

    BreedRepo repo = new BreedRepo();
    await repo.upsert(_breed);

    setState(() {
      _favoriteButtonEnabled = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              _breed.name,
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold
              ),
            ),
          ),

          IconButton(
            icon: Icon(_breed.favorite ? Icons.favorite : Icons.favorite_border),
            onPressed: _favoriteButtonEnabled ? favoriteBreed : null,
            iconSize: 25,
          ),
        ],
      ),
    );
  }
}