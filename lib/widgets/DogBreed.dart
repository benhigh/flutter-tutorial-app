import 'package:flutter/material.dart';
import 'package:my_flutter_app/domain/Breed.dart';
import 'package:my_flutter_app/pages/DogDetails.dart';

class DogBreed extends StatelessWidget {
  final Breed breed;

  DogBreed(this.breed);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          new MaterialPageRoute(
            builder: (context) => new DogDetails(breed)
          )
        );
      },

      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.grey[500],
          )
        ),

        child: Row(
          children: <Widget>[
            Icon(
              breed.favorite ? Icons.star : Icons.star_border,
              color: breed.favorite ? Colors.yellow[700] : Colors.black,
              size: 35,
            ),

            Expanded(
              child: Text(
                breed.name,
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                )
              ),
            ),

            Icon(
              Icons.chevron_right,
              size: 35,
            ),
          ],
        ),
      ),
    );
  }
}